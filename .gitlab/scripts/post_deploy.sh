#!/bin/bash
# post_deploy.sh: for post deployment actions

# exit on errors
set -e

# gitlab ci places us in cloned repo base
WORK_DIR="$(pwd)"

### VARIABLES
. "${WORK_DIR}/.gitlab/scripts/variables.sh"

### SETTINGS VARS
[ "$DEPLOY_DEBUG_MODE" = on ] && RSYNC_DEBUG_PARAMS="--dry-run"

### FUNCTIONS
. "${WORK_DIR}/.gitlab/scripts/functions.sh"

### FUNCTIONS
copy_override_files() {
    local override_basedir="${WORK_DIR}/.gitlab/files/override"
    local override_dir=
    # override dir can be customized by user to avoid conflict when merging
    if [ -d "${override_basedir}/${BRANCH}" ]; then
	override_dir="${override_basedir}/${BRANCH}"
    elif [ -d "${override_basedir}/development" ]; then
	override_dir="${override_basedir}/development"
    elif [ -d "${override_basedir}/staging" ]; then
	override_dir="${override_basedir}/staging"
    elif [ -d "${override_basedir}/master" ]; then
	override_dir="${override_basedir}/master"
    fi
    if [ -d "${override_dir}" ]; then
	# copy the override folder containing config files and such
	deploy_project "$REMOTE_USER" "$REMOTE_SITE" "$REMOTE_DIR" "$override_dir"
    fi
}

post_deploy() {
    REMOTE_USER='none'
    REMOTE_SITE='your-domain'
    REMOTE_DIR="/opt/www/deploy/git/${NAMESPACE}/${PROJECT}/${BRANCH}"
    ### COPY OVERRIDE FILES FOR CONFIG
    copy_override_files
    ### ENABLING APACHE VIRTUALHOST(S)
    CUSTOM_SITES_AVAILABLE_DIR="/home/gitlab-runner/etc/apache2/sites-available"
    CUSTOM_SITES_ENABLED_DIR="/home/gitlab-runner/etc/apache2/sites-enabled"
    APACHE_CONFIG_NAME="${NAMESPACE}-${PROJECT}-${BRANCH}.your-domain.conf"
    APACHE_TEMPLATE_NAME="namespace-project-branch.your-domain.conf"
    APACHE_SSL_CONFIG_NAME="${NAMESPACE}-${PROJECT}-${BRANCH}.your-domain-le-ssl.conf"
    APACHE_SSL_TEMPLATE_NAME="namespace-project-branch.your-domain-le-ssl.conf"
    APACHE_NAMESPACE_COMPAT_CONFIG_NAME="${PROJECT}-${BRANCH}-namespace-compat.your-domain.conf"
    APACHE_NAMESPACE_COMPAT_TEMPLATE_NAME="project-branch-namespace-compat.your-domain.conf"
    APACHE_NAMESPACE_COMPAT_SSL_CONFIG_NAME="${PROJECT}-${BRANCH}-namespace-compat.your-domain-le-ssl.conf"
    APACHE_NAMESPACE_COMPAT_SSL_TEMPLATE_NAME="project-branch-namespace-compat.your-domain-le-ssl.conf"
    # apache template can be customized as well, so check for that
    if [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${NAMESPACE}-${PROJECT}-${BRANCH}.your-domain.conf" ]; then
        APACHE_TEMPLATE_NAME="${NAMESPACE}-${PROJECT}-${BRANCH}.your-domain.conf"
    elif [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${NAMESPACE}-${PROJECT}-branch.your-domain.conf" ]; then
        APACHE_TEMPLATE_NAME="${NAMESPACE}-${PROJECT}-branch.your-domain.conf"
    elif [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/namespace-${PROJECT}-branch.your-domain.conf" ]; then
        APACHE_TEMPLATE_NAME="namespace-${PROJECT}-branch.your-domain.conf"
    elif [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${NAMESPACE}-project-branch.your-domain.conf" ]; then
        APACHE_TEMPLATE_NAME="${NAMESPACE}-project-branch.your-domain.conf"
    fi
    # apache ssl template can be customized as well, so check for that
    if [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${NAMESPACE}-${PROJECT}-${BRANCH}.your-domain-le-ssl.conf" ]; then
        APACHE_SSL_TEMPLATE_NAME="${NAMESPACE}-${PROJECT}-${BRANCH}.your-domain-le-ssl.conf"
    elif [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${NAMESPACE}-${PROJECT}-branch.your-domain-le-ssl.conf" ]; then
        APACHE_SSL_TEMPLATE_NAME="${NAMESPACE}-${PROJECT}-branch.your-domain-le-ssl.conf"
    elif [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/namespace-${PROJECT}-branch.your-domain-le-ssl.conf" ]; then
        APACHE_SSL_TEMPLATE_NAME="namespace-${PROJECT}-branch.your-domain-le-ssl.conf"
    elif [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${NAMESPACE}-project-branch.your-domain-le-ssl.conf" ]; then
        APACHE_SSL_TEMPLATE_NAME="${NAMESPACE}-project-branch.your-domain-le-ssl.conf"
    fi
    ### FOR NAMESPACE COMPATIBILITY!
    # apache namespace compat template can be customized as well, so check for that
    if [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${PROJECT}-${BRANCH}-namespace-compat.your-domain.conf" ]; then
        APACHE_NAMESPACE_COMPAT_TEMPLATE_NAME="${PROJECT}-${BRANCH}-namespace-compat.your-domain.conf"
    elif [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${PROJECT}-branch-namespace-compat.your-domain.conf" ]; then
        APACHE_NAMESPACE_COMPAT_TEMPLATE_NAME="${PROJECT}-branch-namespace-compat.your-domain.conf"
    fi
    # apache namespace compat ssl template can be customized as well, so check for that
    if [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${PROJECT}-${BRANCH}-namespace-compat.your-domain-le-ssl.conf" ]; then
        APACHE_NAMESPACE_COMPAT_SSL_TEMPLATE_NAME="${PROJECT}-${BRANCH}-namespace-compat.your-domain-le-ssl.conf"
    elif [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${PROJECT}-branch-namespace-compat.your-domain-le-ssl.conf" ]; then
        APACHE_NAMESPACE_COMPAT_SSL_TEMPLATE_NAME="${PROJECT}-branch-namespace-compat.your-domain-le-ssl.conf"
    fi
    # now check for enabled site and enable if not
    local reload_apache=1
    if [ -e "${APACHE_SITES_AVAILABLE_DIR}/${APACHE_TEMPLATE_NAME}" ] || [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${APACHE_TEMPLATE_NAME}" ]; then
	if [ -n "${APACHE_TEMPLATE_NAME}" ] && [ ! -e "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_CONFIG_NAME}" ]; then
	    cp "${CUSTOM_SITES_AVAILABLE_DIR}/${APACHE_TEMPLATE_NAME}" "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_CONFIG_NAME}"
	    sed -e "s/{namespace}/${NAMESPACE}/g" -e "s/{project}/${PROJECT}/g" -e "s/{branch}/${BRANCH}/g" -e "s/{port}/${PORT}/g" -i "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_CONFIG_NAME}"
	    reload_apache=0
	fi
    fi
    # check for ssl config; if present enable site if not already enabled
    if [ -e "${APACHE_SITES_AVAILABLE_DIR}/${APACHE_SSL_TEMPLATE_NAME}" ] || [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${APACHE_SSL_TEMPLATE_NAME}" ]; then
        if [ -n "${APACHE_SSL_TEMPLATE_NAME}" ] && [ ! -e "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_SSL_CONFIG_NAME}" ]; then
            cp "${CUSTOM_SITES_AVAILABLE_DIR}/${APACHE_SSL_TEMPLATE_NAME}" "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_SSL_CONFIG_NAME}"
            sed -e "s/{namespace}/${NAMESPACE}/g" -e "s/{project}/${PROJECT}/g" -e "s/{branch}/${BRANCH}/g" -e "s/{port}/${PORT}/g" -i "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_SSL_CONFIG_NAME}"
	    reload_apache=0
        fi
    fi
    ### FOR NAMESPACE COMPATIBILITY!
    if [ "$NAMESPACE" = absas ]; then
	# check for namespace compat config; if present enable site if not already enabled
	if [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${APACHE_NAMESPACE_COMPAT_TEMPLATE_NAME}" ]; then
	    if [ -n "${APACHE_NAMESPACE_COMPAT_TEMPLATE_NAME}" ] && [ ! -e "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_NAMESPACE_COMPAT_CONFIG_NAME}" ]; then
		cp "${CUSTOM_SITES_AVAILABLE_DIR}/${APACHE_NAMESPACE_COMPAT_TEMPLATE_NAME}" "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_NAMESPACE_COMPAT_CONFIG_NAME}"
		sed -e "s/{namespace}/${NAMESPACE}/g" -e "s/{project}/${PROJECT}/g" -e "s/{branch}/${BRANCH}/g" -e "s/{port}/${PORT}/g" -i "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_NAMESPACE_COMPAT_CONFIG_NAME}"
		reload_apache=0
	    fi
	fi
	# check for namespace compat ssl config; if present enable site if not already enabled
	if [ -e "${CUSTOM_SITES_AVAILABLE_DIR}/${APACHE_NAMESPACE_COMPAT_SSL_TEMPLATE_NAME}" ]; then
	    if [ -n "${APACHE_NAMESPACE_COMPAT_SSL_TEMPLATE_NAME}" ] && [ ! -e "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_NAMESPACE_COMPAT_SSL_CONFIG_NAME}" ]; then
		cp "${CUSTOM_SITES_AVAILABLE_DIR}/${APACHE_NAMESPACE_COMPAT_SSL_TEMPLATE_NAME}" "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_NAMESPACE_COMPAT_SSL_CONFIG_NAME}"
		sed -e "s/{namespace}/${NAMESPACE}/g" -e "s/{project}/${PROJECT}/g" -e "s/{branch}/${BRANCH}/g" -e "s/{port}/${PORT}/g" -i "${CUSTOM_SITES_ENABLED_DIR}/${APACHE_NAMESPACE_COMPAT_SSL_CONFIG_NAME}"
		reload_apache=0
	    fi
	fi
    fi
    # apache needs to be reloaded after virtualhost changes
    if [ "$reload_apache" -eq 0 ]; then
	sudo service apache2 reload
    fi
}

# check if branch is allowed
deployment_allowed=1
if [ "$BRANCH" = master ]; then
    deployment_allowed=0
elif [ "$BRANCH" = development ]; then
    deployment_allowed=0
elif [ "$BRANCH" = staging ]; then
    deployment_allowed=0
else
    . .gitlab/files/branches_to_deploy
    for branch_to_deploy in "${BRANCHES_TO_DEPLOY[@]}"; do
	if [ "$BRANCH" = "$branch_to_deploy" ]; then
	    deployment_allowed=0
	fi
    done
fi

if [ "$deployment_allowed" -eq 0 ]; then
    # post deployment can be run
    post_deploy
else
    echo "branch $BRANCH not configured for post deployment"
fi

#  vim: set tabstop=4 shiftwidth=4 :
