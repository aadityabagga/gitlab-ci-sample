#!/bin/bash
# deploy_project.sh: to deploy project

# exit on errors
set -e

# gitlab ci places us in cloned repo base
WORK_DIR="$(pwd)"

### VARIABLES
. "${WORK_DIR}/.gitlab/scripts/variables.sh"

### SETTINGS VARS
[ "$DEPLOY_DEBUG_MODE" = on ] && RSYNC_DEBUG_PARAMS="--dry-run"

### FUNCTIONS
. "${WORK_DIR}/.gitlab/scripts/functions.sh"

# check if branch is allowed
deployment_allowed=1
if [ "$BRANCH" = master ]; then
    deployment_allowed=0
elif [ "$BRANCH" = development ]; then
    deployment_allowed=0
elif [ "$BRANCH" = staging ]; then
    deployment_allowed=0
else
    . .gitlab/files/branches_to_deploy
    for branch_to_deploy in "${BRANCHES_TO_DEPLOY[@]}"; do
	if [ "$BRANCH" = "$branch_to_deploy" ]; then
	    deployment_allowed=0
	fi
    done
fi

## check for review environment
#if [ "$ENVIRONMENT" = "review-$BRANCH-$COMMIT" ]; then
#    deployment_allowed=0
#fi

if [ "$deployment_allowed" -eq 0 ]; then
    # deploy to development server
    REMOTE_USER='none'
    REMOTE_SITE='your-domain'
    REMOTE_DIR="/opt/www/deploy/git/${NAMESPACE}/${PROJECT}/${BRANCH}"

#    if [ "$BRANCH" = staging ]; then
#	REMOTE_DIR="/opt/www/deploy/staging/${NAMESPACE}/${PROJECT}/${BRANCH}"
#    fi

#    # check for review environment
#    if [ "$ENVIRONMENT" = "review-$BRANCH-$COMMIT_ID" ]; then
#	REMOTE_DIR="/opt/www/deploy/review/${NAMESPACE}/${PROJECT}/${BRANCH}/${COMMIT}"
#    fi

    # Check for push on production!
    if [ "$BRANCH" = staging ] && [ "$ENVIRONMENT" = production ]; then
	. "${WORK_DIR}/.gitlab/files/.live_deployment_params"
    fi

    deploy_project "$REMOTE_USER" "$REMOTE_SITE" "$REMOTE_DIR" "$WORK_DIR"
else
    echo "branch $BRANCH not configured for deployment"
fi
