#!/bin/bash
# test_project.sh: to test project

# exit on errors
set -e

# gitlab ci places us in cloned repo base
WORK_DIR="$(pwd)"

# we get project and branch as environment variables set by gitlab ci
PROJECT="$CI_PROJECT_NAME"
branch="$CI_COMMIT_REF_NAME"

# check if branch is allowed
deployment_allowed=1
if [ "$branch" = master ]; then
    deployment_allowed=0
elif [ "$branch" = development ]; then
    deployment_allowed=0
else
    . .gitlab/files/branches_to_deploy
    for branch_to_deploy in "${BRANCHES_TO_DEPLOY[@]}"; do
	if [ "$branch" = "$branch_to_deploy" ]; then
	    deployment_allowed=0
	fi
    done
fi

if [ "$deployment_allowed" -eq 0 ]; then
    # tests can be run
    newman run api/test/App_Git.postman_collection.json -e api/test/env/$branch.postman_environment.json --suppress-exit-code
else
    echo "branch $branch not configured for testing"
fi
